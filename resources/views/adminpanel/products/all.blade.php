@extends('adminpanel.layouts.master')

@section('content')
<a class="btn btn-success" href="{{ route('admin.products.add') }}">Add a Product</a>
<br>
<div class="row">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Category</th>
				<th>Price</th>
				<th>Created</th>
				<th>Updated</th>
				<th>Active</th>
			</tr>
		</thead>
		<tbody>
			@foreach($products as $product)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $product->title }}</td>
				<td>{{ $product->description }}</td>
				<td>{{ $product->category }}</td>
				<td>{{ $product->price }}</td>
				<td>{{ $product->created_at }}</td>
				<td>{{ $product->updated_at }}</td>
				<td>
					@if($product->active)
					<a class="btn btn-sm btn-success" href="#">Active</a>
					@else
					<a class="btn btn-sm btn-danger" href="#">De-active</a>
					@endif
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection