@extends('adminpanel.layouts.master')

@section('content')

<a class="btn btn-default" href="{{ route('admin.products.all') }}">Go Back (Products)</a>
<br>
<div class="row">
	<form method="post" class="form-horizontal" action="{{ route('admin.products.add') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" id="title">
		</div>

		<div class="form-group">
			<label for="category">Category</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->title }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label for="category">Price</label>
			<input class="form-control" type="number" name="price" min="0">
		</div>

		<div class="form-group">
			<label for="image-link">Featured Image</label>
			<input type="file" name="image_link">
		</div>

		<div class="form-group">
			<label for="description">Description(optional)</label>
			<textarea id="description" name="description" class="form-control"></textarea>
		</div>

		<button type="submit" class="btn btn-primary" name="submit">Save</button>
	</form>
	
</div>


@endsection