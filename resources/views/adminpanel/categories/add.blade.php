@extends('adminpanel.layouts.master')

@section('content')

<a class="btn btn-default" href="{{ route('admin.categories.all') }}">Go Back (Categories)</a>
<br>
<div class="row">
	<form method="post" class="form-horizontal" action="{{ route('admin.categories.add') }}">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" id="title">
		</div>

		<div class="form-group">
			<label for="description">Description(optional)</label>
			<textarea id="description" name="description" class="form-control"></textarea>
		</div>

		<button type="submit" class="btn btn-primary" name="submit">Save</button>
	</form>
	
</div>


@endsection