@extends('adminpanel.layouts.master')

@section('content')

<a class="btn btn-success" href="{{ route('admin.categories.add') }}">Add a Category</a>
<br>
<div class="row">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Created</th>
				<th>Updated</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categories as $category)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $category->title }}</td>
				<td>{{ $category->description }}</td>
				<td>{{ $category->created_at }}</td>
				<td>{{ $category->updated_at }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection