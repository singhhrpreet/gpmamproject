@extends('adminpanel.layouts.master')

@section('content')
<div class="row">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>Created</th>
				<th>Updated</th>
				<th>Active</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->created_at }}</td>
				<td>{{ $user->updated_at }}</td>
				<td>
					@if($user->active)
					<a class="btn btn-sm btn-success" href="#">Active</a>
					@else
					<a class="btn btn-sm btn-danger" href="#">De-active</a>
					@endif
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection