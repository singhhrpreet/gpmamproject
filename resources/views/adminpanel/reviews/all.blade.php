@extends('adminpanel.layouts.master')

@section('content')
<div class="row">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Review</th>
				<th>Product</th>
				<th>Author</th>
				<th>Created</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($reviews as $review)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $review->title }}</td>
				<td>{{ $review->review }}</td>
				<td>{{ $review->product }}</td>
				<td>{{ $review->reviewer }}</td>
				<td>{{ $review->created_at }}</td>
				<td>
					@if($review->allowed_by_admin)
					<a class="btn btn-sm btn-success" href="{{ route('admin.reviews.togglestatus', ['id' => $review->id]) }}">Allowed</a>
					@else
					<a class="btn btn-sm btn-danger" href="{{ route('admin.reviews.togglestatus', ['id' => $review->id]) }}">Not Allowed</a>
					@endif
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection