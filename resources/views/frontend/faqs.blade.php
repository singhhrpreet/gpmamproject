@extends('frontend.layouts.master')

@push('styles')

<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc; 
}

.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
    overflow: hidden;
}
</style>

@endpush

@section('content')
<div class="row">
	<div class="col-lg-12">
		<h2>FAQ LIST</h2>
		<button class="accordion">HOW CAN I CHANGE MY SHIPPING ADDRESS?</button>
		<div class="panel">
			<p>By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.</p>
		</div>

		<button class="accordion">WHY MUST I MAKE PAYMENT IMMEDIATELY AT CHECKOUT?</button>
		<div class="panel">
		  <p>Sample ordering is on �first-come-first-served� basis. To ensure that you get your desired samples, it is recommended that you make your payment within 60 minutes of checking out.</p>
		</div>

		<button class="accordion">HOW DO I CANCEL MY ORDERS BEFORE I MAKE A PAYMENT?</button>
		<div class="panel">
		  <p>After logging into your account, go to your Shopping Cart. Here, you will be able to make payment or cancel your order. Note: We cannot give refunds once payment is verified.</p>
		</div>
		<button class="accordion">HOW LONG WILL IT TAKE FOR MY ORDER TO ARRIVE AFTER I MAKE PAYMENT?</button>
		<div class="panel">
		  <p>Members who ship their orders within Singapore should expect to receive their orders within five (5) to ten (10) working days upon payment verification depending on the volume of orders received.

		If you experience delays in receiving your order, contact us immediately and we will help to confirm the status of your order.</p>
		</div>
		<button class="accordion">WHAT HAPPENS IF THERE'S BEEN A DELIVERY MISHAP TO MY ORDER? (DAMAGED OR LOST DELIVERY)</button>
		<div class="panel">
		  <p>We take such matters very seriously and will look into individual cases thoroughly. Any sample that falls under the below categories should not be thrown away before taking photo proof and emailing the photo of the affected sample and your D.O (Delivery Order) to us at help@samplestore.com (if applicable).

		We regret to inform you that no refunds will be given for orders that fall under the below categories.

		1. In the event of damaged samples received, we will require photo proof of the affected samples and your D.O (Delivery Order) in order for us to investigate and review before a decision is made to re-send the sample to you at no cost, subject to availability. In light of this, any sample that falls into this category should not be thrown away before taking photo proof and emailing the photo to us at help@samplestore.com

		2. In the event of lost mail, we will try to locate the delivery team in Singpost and if there's a clear indication that your order is indeed lost, we'll re-send the order to you at no cost, subject to availability.</p>
		</div>

	</div>
		
</div>

@endsection

@push('scripts')
<script type="text/javascript">
accordion = document.getElementsByClassName("accordion");

for (i = 0; i < accordion.length; i++) {
    accordion[i].addEventListener("click", function() {
        this.classList.toggle("active");
        sibling = this.nextElementSibling;
        if (sibling.style.display === "block") {
            sibling.style.display = "none";
        } else {
            sibling.style.display = "block";
        }
    });
}
</script>
@endpush