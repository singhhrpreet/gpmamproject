@extends('frontend.layouts.master')

@section('content')
	<h5><center>You're now reviewing <strong>{{ $product->title }}</strong></center></h5>
	<div class="row justify-content-center">
		<div class="col-lg-6 col-lg-3-offset">
			<form class="form-horizontal" method="post" action="{{ route('review.create', ['product_id' => $product->id]) }}">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="rating">Rating <span rating-view></span> out of 10</label>
					<input class="form-control" type="range" name="rating" min=0 max=10></div>
				<div class="form-group">
					<label for="title">Headline of your Review</label>
					<input type="text" class="form-control" name="title">
				</divr>
				<div class="form-group">
					<label for="description">Details of your Review</label>
					<textarea class="form-control" name="review"></textarea>
				</div>
				<button type="submit" class="btn btn-success">Save</button>
			</form>
		</div>
	</div>
@endsection

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('[rating-view]').text($('[name="rating"]').val());

		$('[name="rating"]').change(function() {
			$('[rating-view]').text(this.value);
		})
	})
</script>
@endpush