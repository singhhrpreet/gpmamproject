<div class="footer">
	<hr>
	<div class="container">
	    <div class="row">
	          <div class="col-lg-3 col-md-6 col-sm-12 links-section">
	                <h4>About</h4>
	                <ul>
	                      <li><a href="#" target="_blank">Link 1</a></li>
	                      <li><a href="#" target="_blank">Link 2</a></li>
	                      <li><a href="#" target="_blank">Link 3</a></li>
	                      <li><a href="#" target="_blank">Link 4</a></li>
	                </ul>
	          </div>
	          <div class="col-lg-3 col-md-6 col-sm-12 links-section">
	                <h4>Policies</h4>
	          </div>
	          <div class="col-lg-3 col-md-6 col-sm-12 links-section">
	                <h4>Returns</h4>
	          </div>
	          <div class="col-lg-3 col-md-6 col-sm-12 links-section">
	                <h4>Contact Us</h4>
	          </div>
	    </div>
	</div>
	<hr>
	<div class="d-flex justify-content-center copyright-section">
		<p><i class="fas fa-copyright"></i> 2012-2018 Alpha Coders</p>
	</div>
</div>