<div class="side-nav">
  <a href="#" id="side-nav-close" class="nav-close-btn float-right"><i class="fas fa-times fa-lg"></i></a>
  <ul>
    <li><a href="{{ route('product.all') }}">Products</a></li>
    <li><a href="{{ route('faq') }}">FAQ</a></li>
    @if(\Auth::check())
    @else
    <li><a href="{{ route('login') }}">Log In</a></li> 
    <li><a href="{{ route('register') }}">Sign Up</a></li>
    @endif
  </ul>
</div>

<div class="nav-container navbar-light bg-light fixed-top">

  <div class="brand-bar nav justify-content-center">
    <h4>{{ config('app.name', 'Laravel')}}</h4>
  </div>

  <nav class="d-flex justify-content-between">

    <div>
      <button class="btn btn-outline-primary" id="side-nav-open"><i class="fas fa-bars"></i></button>
    </div>
    <div class="search-bar">
      <form method="get" action="{{ route('product.all') }}">
        <input type="search" name="q" class="form-control" size="70" value="{{ $search_term }}" placeholder="Type your search and Hit Enter">
        <button type="submit" class="btn btn-outline-primary"><i class="fas fa-search"></i></button>
      </form>
    </div>
    <div>
      <button title="Logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-outline-primary"><i class="fa fa-sign-out-alt"></i></button>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
      </form>
    </div>
    
  </nav>

</div>