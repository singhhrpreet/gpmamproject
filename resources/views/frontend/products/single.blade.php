@extends('frontend.layouts.master')

@section('content')
<!-- <div class="row">
<div class="col-12">
<ol class="d-flex justify-content-lg-start breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item"><a href="#">Library</a></li>
<li class="breadcrumb-item active" aria-current="page">Data</li>
</ol>
</div>
</div> -->

<div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12 product-image-section">
            <div class="product-image">
                  <a href="#"><img src="{{ asset('storage/'.$product->image_link) }}" alt="" class="product-img"></a>
            </div>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 product-info-section">
            <div class="card">
                  <div class="d-flex justify-content-between card-header">
                        <div class="p">{{ $product->title }}</div>
                        <div><a href="#"><p><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star-half-alt"></i> {{ count($reviews) }} Ratings & Review(s)</p></a></div>
                  </div>
                  <div class="card-body">
                        <h5 class="card-title is-price">Now at {{ $product->price }} </h5>
                        
                        <div class="card-text">
                              <br>
                              <h5>Description:</h5>
                              <p>
                              {{ $product->description }}
                              </p>
                                    
                        </div>
                  </div>
            </div>
      </div>
</div>

<div class="row">
      <div class="col-lg-12">
            <h4>Showing {{ count($reviews) }} review(s) <a href="{{ route('review.create', ['product_id' => $product->id]) }}" class="btn btn-primary">Write a Review</a></h4>
            @foreach($reviews as $review)
            <div class="card">
                  <div class="card-body">
                        <h5 class="card-title">{{ $review->title }} <small class="text-muted">by: {{ $review->reviewer }} (<a href="mailto:">{{ $review->reviewer_mail_id }}</a>)</small></h5>
                        <h5>{{ $review->rating }} out of 10</h5>
                        <p class="card-text">{{ $review->review }}</p>
                  </div>
            </div>
            @endforeach
      </div>
</div>
@endsection