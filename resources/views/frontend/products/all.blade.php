@extends('frontend.layouts.master', ['search_term' => $search_term])

@section('content')
	<div class="row">
            <div class="col-lg-2">
                  <div class="filter-side-bar">
                        <div id="filters">
                              <div class="filter-name">
                                    <a href="#" class="filter-btn" data-toggle="collapse" data-target="#filter-section">

                                          <div id="filter-heading" class="filter-heading">
                                                <h4>Filters</h4>    
                                          </div>

                                    </a>
                                    <div id="filter-section" class="collapse show filter-options">
                                          <div class="filter-name">
                                                <a href="#" class="filter-btn" data-toggle="collapse" data-target="#brands">

                                                      <div id="filter-heading" class="filter-heading">
                                                            <h4>Categories</h4>    
                                                      </div>

                                                </a>
                                                <div id="brands" class="collapse filter-options">

                                                      <ul>
                                                            @foreach($categories as $category)
                                                            <li><input type="checkbox">{{ $category->title}}</li>
                                                            @endforeach
                                                      </ul>

                                                </div>
                                          </div>
                                          <!-- Add other filter div here.. -->
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <div class="col-lg-10 col-sm-12">
                  <div class="product-listing-section">
                        <div class="row">
                              @foreach($products as $product)
                              <div class="col-lg-3 col-md-4 col-sm-6">
                                    <a href="{{ route('product.single', ['id' => $product->id ]) }}">
                                          <div class="d-flex flex-column align-items-center product-list-item">
                                                <div>

                                                      <img src="{{ asset('storage/'.$product->image_link) }}" alt="" class="product-img">

                                                </div>
                                                <div class="d-flex flex-column align-items-center">
                                                      <p>{{ $product->title }}</p>
                                                      <p>{{ $product->price }}</p>
                                                      <!-- <p>Rating 3/5</p> -->
                                                </div>
                                          </div>
                                    </a>
                              </div>
                              @endforeach
                        </div>
                  </div>
            </div>
      </div>
@endsection