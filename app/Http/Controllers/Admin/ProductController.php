<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function all()
    {
    	$products = \App\Product::join('product_categories', 'products.category_id', 'product_categories.id')
    	->select(
    		'products.*',
    		'product_categories.title as category'
    	)->get();

    	return view('adminpanel.products.all')->with('products', $products);

    }

    public function single()
    {

    }

    public function addPage()
    {
    	$categories = \App\ProductCategory::all();

    	return view('adminpanel.products.add')->with('categories', $categories);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1|max:255',
            'category_id' => 'numeric|required',
            'price' => 'numeric|required|min:0',
        ]);

        $file = $request->image_link->store('public');

        $file = explode("/", $file);

        $file = $file[1];

        \App\Product::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'price' => $request->input('price'),
            'image_link' => $file
        ]);

        return redirect()->route('admin.products.all')->with([
            'status' => 1,
            'message' => 'Successfully Added a New Product'
        ]);
    }
}
