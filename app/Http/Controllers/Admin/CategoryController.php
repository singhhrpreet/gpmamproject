<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function all()
    {

    	$categories = \App\ProductCategory::all();
    	return view('adminpanel.categories.all', ['categories' => $categories]);

    }

    public function addPage()
    {
    	return view('adminpanel.categories.add');
    }

    public function add(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required|min:1|max:255'
    	]);



    	\App\ProductCategory::create([
    		'title' => $request->input('title'),
    		'description' => $request->input('description')
    	]);

    	return redirect()->route('admin.categories.all')->with([
    		'status' => 1,
    		'message' => 'Successfully Added a New Category'
    	]);
    }
}
