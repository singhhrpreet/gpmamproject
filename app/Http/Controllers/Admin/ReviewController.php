<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    //
    public function all()
    {
    	$reviews = \App\ProductReview::join('products', 'products.id', 'product_reviews.product_id')
    	->join('users', 'users.id', 'product_reviews.user_id')
    	->select(
    		'product_reviews.*',
    		'users.name as reviewer',
    		'products.title as product'
    	)
    	->orderBy('id', 'DESC')
    	->get();

    	return view('adminpanel.reviews.all', ['reviews' => $reviews]);

    }

    public function toggleStatus($id = "")
    {
    	$review = \App\ProductReview::find($id);

    	$review->allowed_by_admin = !$review->allowed_by_admin;

    	$review->save();

    	return back()->with([
    		'status' => 1,
    		'message' => 'Product Review status Updated'
    	]);
    }
}
