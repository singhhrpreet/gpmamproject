<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function all()
    {
    	$users = \App\User::all();

    	return view('adminpanel.users.all')->with('users', $users);

    }
}
