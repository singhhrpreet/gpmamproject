<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductReviewController extends Controller
{
    public function createPage($product_id = "")
    {
    	$product = \App\Product::find($product_id);
    	return view('frontend.review.create', ['product' => $product]);

    }

    public function create(Request $request, $product_id)
    {

    	// dd($request->all());
    	$this->validate($request, [
    		'title' => 'required',
    		'review' => 'required',
    		'rating' => 'numeric|min:0|max:10'
    	]);

    	\App\ProductReview::create([
    		'product_id' => $product_id,
    		'title' => $request->input('title'),
    		'rating' => $request->input('rating'),
    		'review' => $request->input('review'),
    		'user_id' => \Auth::user()->id
    	]);

    	return redirect()->route('product.single', ['product_id' => $product_id])->with([
    		'status' => 1,
    		'message' => 'Successfully added the Review! Waiting for the Admin to review and allow.'
    	]);
    }
}
