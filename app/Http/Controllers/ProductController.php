<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    
    public function all($search_term = "", Request $request)
    {
        $search_term = $search_term ? $search_term : $request->input('q');  
        
        if($search_term) {

            $pattern = "%" . $search_term . "%";
            
            $products = \App\Product::where('title', 'LIKE', $pattern)->orWhere('description', 'LIKE', $pattern)->get();
        } else {

            $products = \App\Product::all();
        }


    	$categories = \App\ProductCategory::all();

    	return view('frontend.products.all')
        ->with('products', $products)
        ->with('categories', $categories)
        ->with('search_term', $search_term);
    }

    public function single($id = '')
    {
    	$product = \App\Product::find($id);
        $reviews = \App\ProductReview::join('users', 'product_reviews.user_id', 'users.id')
        ->select(
            'product_reviews.*',
            'users.name as reviewer',
            'users.email as reviewer_mail_id'
        )
        ->where('product_id', $id)
        ->where('allowed_by_admin', 1)
        ->get();

    	return view('frontend.products.single')
        ->with('product', $product)
        ->with('reviews', $reviews);
    }

    // public function search($term = "")
    // {
    //     $pattern = "%" . $term . "%";
        
    //     $products = \App\Product::where('title', 'LIKE', $pattern)->orWhere('description', 'LIKE', $pattern)->get();

    //     $categories = \App\ProductCategory::all();

    //     return view('frontend.products.all')->with('products', $products)->with('categories', $categories);
    // }
}
