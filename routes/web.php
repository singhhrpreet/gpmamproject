<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('product.all');
});

Auth::routes();

Route::get('/products/{term?}', 'ProductController@all')->name('product.all');
Route::get('/product/{id?}', 'ProductController@single')->name('product.single');
// Route::get('/products/search/{term}', 'ProductController@search')->name('product.search');

Route::group(['middleware' => 'auth'], function() {

	Route::get('/review/create/{product_id}', 'ProductReviewController@createPage')->name('review.create');
	Route::post('/review/create/{product_id}', 'ProductReviewController@create')->name('review.create');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/faq', 'HomeController@faq')->name('faq');


Route::group(['middleware' => 'notadmin', 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function() {

	Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/login', 'LoginController@login')->name('login');

});

Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function() {

	Route::get('/', 'HomeController@index')->name('admin.index');

	Route::get('/products', 'ProductController@all')->name('products.all');
	Route::get('/product/{id}', 'ProductController@single')->name('products.single');

	Route::get('/products/add', 'ProductController@addPage')->name('products.add');
	Route::post('/products/add', 'ProductController@add')->name('products.add');

	Route::get('/product/activetoggle/{id}', 'ProductController@activetoggle')->name('products.activetoggle');

	Route::get('/categories', 'CategoryController@all')->name('categories.all');
	Route::get('/catgeory/{id}', 'CategoryController@single')->name('categories.single');

	Route::get('/categories/add', 'CategoryController@addPage')->name('categories.add');
	Route::post('/categories/add', 'CategoryController@add')->name('categories.add');
	
	Route::get('/users', 'UserController@all')->name('users.all');
	
	Route::get('/reviews', 'ReviewController@all')->name('reviews.all');
	Route::get('/review/{id}', 'ReviewController@single')->name('reviews.single');
	Route::get('/review/togglestatus/{id}', 'ReviewController@toggleStatus')->name('reviews.togglestatus');
	
	Route::post('/logout', 'LoginController@logout')->name('logout');
});

