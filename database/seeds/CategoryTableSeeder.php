<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('product_categories')->insert([
        	[
        		'title' => 'Mobile Phones',
        		'description' => 'This category is for Mobile Phones',
        	],
        	[
        		'title' => 'Tablets',
        		'description' => 'This category is for Tablets'
        	]
        ]);
    }
}
