<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Admin::create([
        	'name' => 'Admin Bhaaji',
        	'email' => 'admin@admin.com',
        	'password' => bcrypt('pass1234')
        ]);
    }
}
